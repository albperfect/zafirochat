const $ = require('jquery');
const ipc = require('electron').ipcRenderer 




$(document).mousemove(function(event){
    $(".animable").removeClass("wrong-entry");
});

var menustate = false
function di(id){
    return document.getElementById(id); 
}
function print(msg){
    ipc.send("print",msg);
}


function enviar(){
    $(".animable").removeClass("wrong-entry");
    if ($("#mensaje").val() == "" || $("#mensaje").val() == null){

        $(".animable").addClass("wrong-entry")
        $(".animable").one("transitionend",
        function(event) {
            console.log("animacion terminada");
            $(".animable").removeClass("wrong-entry");
        });
    }else{

        $("<a>",{
            class: "recv1 ",
            text: $("#mensaje").val()
        }).appendTo("#msgs");
        ipc.send("envi",$("#mensaje").val())
        $("#mensaje").val("")
        $("html, body").animate({ scrollTop: $('#mensaje').offset().top }, 1000);
    }
}

function entrar(e){
    if(e.keyCode == 13){
        enviar();
        return false;
    }
}
$(document).ready(function(){
    var cabezeraTop 
    var cabezeraDown
    cabezeraTop = $('#cabezera').offset().top;
    cabezeraDown = $(window).height() - $(".controles").height();
    $(window).scroll(function(){
    
        if($(window).scrollTop() > cabezeraTop){
            $("#cabezera").addClass('sticky')
        }else{
            $("#cabezera").removeClass('sticky')
        }
        
     
    }); 
});


ipc.on("new_msg",(even,arg)=>{
    //var regdata = /^\n--\[.*\]--\n$/
    var regdata = /^.*>.*/
    if (!regdata.test(arg)){
        console.log("mensaje info")
        $("<a>",{
            class: "send2",
            text: arg
        }).appendTo("#msgs");
    }else{

        $("<a>",{
            class: "send1",
            text: arg
        }).appendTo("#msgs");
    }
    $("#mensaje").val("")
    $("html, body").animate({ scrollTop: $('#mensaje').offset().top }, 1000);
});

function openmenu(){
    document.getElementById("menuconfig").style.width = "250px";
    //document.getElementById("central").style.marginLeft = "250px";

    menustate = true;
}
function closemenu(){
    document.getElementById("menuconfig").style.width = "0px";
    //document.getElementById("central").style.marginLeft = "0";
    menustate = false;
}   

function actmenu(x){
    x.classList.toggle("change");
    if (menustate){
        closemenu();
   } else{
        openmenu();
        
   }
}
ipc.on("menu", ()=>{
    $(".container").toggleClass("change");
   if (menustate){
        closemenu();
   } else{
        openmenu();
        
   }
});
function changecss(){

    if (theme){

        di("theme").href = "./index2.css"
        theme = false;
    }else{
        di("theme").href = "./index.css"
        theme = true;
    }
}
