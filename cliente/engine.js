const $ = require('jquery');
const ipc = require('electron').ipcRenderer; 
const remote = require('electron').remote;




var menustate = false;
var theme = true;


function di(id){
    return document.getElementById(id); 
}
function print(msg){
    ipc.send("print",msg);
}
function openmenu(){
    document.getElementById("menuconfig").style.width = "250px";
    //document.getElementById("central").style.marginLeft = "250px";

    menustate = true;
}
function closemenu(){
    document.getElementById("menuconfig").style.width = "0px";
    //document.getElementById("central").style.marginLeft = "0";
    menustate = false;
}   
function actmenu(x){
    x.classList.toggle("change");
    if (menustate){
        closemenu();
   } else{
        openmenu();
        
   }
}
ipc.on("menu", ()=>{
    $(".container").toggleClass("change");
   if (menustate){
        closemenu();
   } else{
        openmenu();
        
   }
});
function changecss(){
    if (theme){

        di("theme").href = "./index2.css"
        theme = false;
    }else{
        di("theme").href = "./index.css"
        theme = true;
    }
}


function entrar(){
    $(".animable").removeClass("wrong-entry");
    if ($("#direc").val() == "" || $("#direc").val() == null){
        console.log("entrada vacia");
        $(".animable").addClass("wrong-entry")
        $(".animable").one("transitionend",
        function(event) {
            console.log("animacion terminada");
            $(".animable").removeClass("wrong-entry");
        });
     
    }else{

        ipc.send("nombre",$("#direc").val());
    }
}

ipc.on("stateupd", (even, arg)=>{
    if (arg == "offline"){
        di("state").innerHTML = "cloud_off"
    }else if(arg == "Conectado"){
        di("state").innerHTML = "cloud_queue"
    }else if(arg == "Logeado"){
        di("state").innerHTML = "cloud"
    }else{
        di("state").innerHTML = "help"
    }
});
ipc.on("load",(even, arg)=>{
    if (arg){
        di("loadicon").style.display = "inline";
    }else{
        di("loadicon").style.display = "none";
    }
})

$(document.body).ready(()=>{
    $("#c_direccion").val(remote.getGlobal('ip'))
    $("#c_direccion").blur(()=>{
        console.log("foco perdido");
        ipc.send("new_ip", $("#c_direccion").val());
        $("#c_direccion").val(remote.getGlobal('ip'))
    
    });
    $("#direc").focus(()=>{
        $(".placeholder").addClass("active");
    });
    $("#direc").blur(()=>{
        if ($("#direc").val() == ""){

            $(".placeholder").removeClass("active");
        }
    });

});

