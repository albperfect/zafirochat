const {app, BrowserWindow, globalShortcut, dialog, Notification} = require('electron');
const ipc = require('electron').ipcMain
const net = require("net");


let Nom = ""
let Modo = "offline"
let IP = "localhost"
global.ip = IP;
let win;
function createWindow(){
    win = new BrowserWindow({
        title: "ZaF",
        titleBarStyle: 'hidden',
        // frame: false,
        width: 800,
        height: 600,
        icon: 'sapphire.png'
        
    })
    win.setMenu(null);
    //win.loadFile("index.html");
    win.loadFile("index.html")
    
}

app.on('ready',()=>{
    createWindow();
    globalShortcut.register('CommandOrControl+D', ()=>{
        win.openDevTools();
    });
    globalShortcut.register('CommandOrControl+W', ()=>{
        Modo = "offline"
        win.webContents.send("stateupd",Modo)
        win.close();
    });
    globalShortcut.register('CommandOrControl+A', ()=>{
        win.webContents.send("menu",null);
    });

});

ipc.on('print', (even, arg)=>{
    console.log(arg);    
});

ipc.on("new_ip",(even, arg)=>{
    IP = arg;
    global.ip = IP;
    console.log("IP cambaida a " + IP );
    win.webContents.send("load",true)
    cliente.setTimeout(10000, function() {
        cliente.connect(3030, IP,()=>{win.webContents.send("load",false)});

    });
});

var cliente = new net.Socket();


cliente.connect(3030, IP, function(){
    console.log("Conectado");
    
    Modo = "Conectado"
    win.webContents.send("load",false)
    win.webContents.send("stateupd",Modo)
});


cliente.on('data', (data)=>{
    console.log(data.toString())

    if (Modo == "Logeado"){

        win.webContents.send("new_msg",data.toString());
        
        let notif = new Notification({
            title: "ZaF",
            subtitle: "Mensaje nuevo",
            body: data.toString(),
            icon: "sapphire.jpg"
        });
        if (!win.focused){

            notif.show();
        }
    }
});
cliente.on("close", ()=>{
    /*dialog.showMessageBox(win,{
        type: "info",
        title: "ZaF",
        message: "Se a perdido la conexion con el servidor",
        buttons: ["Acceptar"]
    },()=>{
        win.loadFile("index.html")
        Modo = "offline"
        win.webContents.send("load",true)
        win.webContents.send("stateupd",Modo)
        
    });*/


});
ipc.on("nombre", (even ,arg)=>{
    Nom=arg
    if (Modo == "Conectado"){
        console.log(Nom)
        cliente.write(Nom+"\n")
        
    
                win.loadFile("chat.html")
                Modo = "Logeado";
                win.webContents.send("stateupd",Modo)
            
    
    }else if(Modo == "offline"){
        win.webContents.send("load",true)
        dialog.showMessageBox(win,{
            type: "error",
            message: "No se a podido conectar con el servidor",
            title: "Zaf",
            buttons: ["Acceptar"]

    });
    }
});
ipc.on("envi",(even, arg)=>{
    //enviar
    if (Modo == "Logeado"){
        console.log(arg);
        cliente.write(arg+"\n")
    }
});
cliente.on("error", ()=>{
    console.log("error al conectar");
    win.webContents.send("load",true)
    cliente.setTimeout(10000, function() {
        cliente.connect(3030, IP,()=>{win.webContents.send("load",false)});

    });
});