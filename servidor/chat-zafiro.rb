 require 'socket'

begin
	require 'colorize'
	COLORED = true
rescue LoadError 
	COLORED = false
end
def msg(text,code)
	case code
		
	when 1
		#normal
		puts "[#{text[0].upcase}]#{text}"
	when 2
		#error
		puts (COLORED ? "["+"!".red.bold+"]#{text}" : "[!]#{text}") 
	when 3
		#OK
		puts (COLORED ? "["+"*".green.bold+"]#{text}" : "[*]#{text}" )
	when 4
		#pregunta
		puts (COLORED ? "["+"?".yellow.bold+"]#{text}" : "[?]#{text}")
	when 5
		#info
		puts (COLORED ? "["+"i".blue.bold.italic+"]#{text}" : "[i]#{text}")
	end
end

#variables
PORT = 3030
IP = Socket.ip_address_list[1].ip_address

clientesnom = [] 
clientes = {}
begin
	servicio = TCPServer.new PORT
	msg("servidor iniciado en " + (COLORED ? IP.to_s.bold.blue + ":".bold + PORT.to_s.bold.blue :  IP.to_s+":"+PORT.to_s )  ,3)

	loop do |contador|
		Thread.start(servicio.accept) do |cliente|
			admin = false
			nom = ""
			msg("cliente #{cliente.addr[2]} " + (COLORED ? "conectado".green : "conectado" ),5)
			cliente.puts "BIENVENDIO a zafirochat"
			loop do
				cliente.puts "Nombre:"
				nom = cliente.gets.chomp
				break if not clientesnom.include?(nom)
				puts "Ya existe ese usuario"
				cliente.puts "Ya existe ese usuario"
			end 
			msg("cliente #{cliente.addr[2]} logeado como " + (COLORED ? nom.bold.blue : nom),5)
			clientesnom.push nom
			clientes[nom] = cliente
			cliente.puts "numero de clientes: #{clientes.length}"
			if cliente.addr[2].to_s == "127.0.0.1" then 
				 cliente.puts "Bienvenido administrador"
				 admin = true
			end
			clientesnom.each do |cs|
					if cs != nom then 
						clientes[cs].puts "\n--[#{nom} se a conectado]--"
					end
				end
			
			while (cliente.gets)
				if admin and $_[0] == "!" then 
					#comando
					cmd = $_.gsub("\n","")
					cmd = cmd.gsub("!","")
					cmd = cmd.split(" ")
					case cmd[0]
						when "sendpriv"
							clientes[cmd[1]].puts "\n[mp@#{nom}]> #{cmd[2..cmd.length].join(" ")}" 
						when "list"
							cliente.puts "\n--[clientes conectados]--"
							clientesnom.each do |cs|
								cliente.puts "\n#{clientes[cs].addr[2]} -> #{cs}"
							end 
							cliente.puts "\n-----------[]-----------"
						when "ban"
							clientes[cmd[1]].puts "--Has sido baneado]--"
							clientes[cmd[1]].close()
							clientes.delete(cmd[1])
						when "test"
							cliente.puts "\n--[El sistema de comandos funciona]--"
						when "promo"
							cmd 
						else 
							cliente.puts "\n--[comando #{cmd[0]} no reconocido]--"

					end
				else
					clientesnom.each do |cs|
						if cs != nom then 
							clientes[cs].puts "#{nom}> #{$_}"
						end
					end
				end
			end
			msg("cliente #{cliente.addr[1,2]} " + (COLORED ? "desconectado".yellow : "desconectado" ),5)
			clientesnom.each do |cs|
					if cs != cliente then 
						clientes[cs].puts "\n--[#{nom} se desconecto]--"
					end
				end
			clientes.delete(clientes[nom])
			clientesnom.delete(nom)
			cliente.close
			
		end
	end 

rescue SignalException => e
	
		msg("Cerrando servidor",1)
  		clientesnom.each do |cs|
  			clientes[cs].puts "\n--[se a cearrado el servidor , gracias por utilizar albperfect solutions;) ]--"
  			clientes[cs].close()
  

  end
clientes.clear 
rescue Exception => e
  msg((COLORED ? "ERROR".red.bold : "ERROR"  ) + " #{e}",2)

end